import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DomePageComponent } from './dome-page.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [DomePageComponent]
})
export class DomePageModule { }
