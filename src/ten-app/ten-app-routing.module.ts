import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DomePageComponent } from 'src/projects/dome-page/dome-page.component';
import { TenAppRouterOutLetComponent } from './ten-app-router-outlet-component';

const routes: Routes = [
  {
    path: 'ten',
    component: TenAppRouterOutLetComponent,
    children: [
      {
        path: 'dome-page',
        component: DomePageComponent,
      }
    ]
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class TenAppRoutingModule { }
