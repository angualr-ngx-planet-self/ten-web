import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { TenAppRoutingModule } from './ten-app-routing.module';
import { TenAppComponent } from './ten-app.component';
import { TenAppRouterOutLetComponent } from './ten-app-router-outlet-component';
import { ProjectsModule } from 'src/projects/projects.module';

@NgModule({
  declarations: [
    TenAppComponent, // root 组件
    TenAppRouterOutLetComponent // 子应用输出组件
  ],
  imports: [
    BrowserModule,
    TenAppRoutingModule,
    ProjectsModule
  ],
  providers: [],
  bootstrap: [TenAppComponent]
})
export class TenAppModule { }
