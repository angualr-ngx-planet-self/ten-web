import { Component } from '@angular/core';

@Component({
  selector: 'ten-app-router-outlet',
  template: '<router-outlet></router-outlet>'
})
export class TenAppRouterOutLetComponent {
}
