import { Component } from '@angular/core';

@Component({
  selector: 'ten-app-root',
  templateUrl: './ten-app.component.html',
  styleUrls: ['./ten-app.component.scss']
})
export class TenAppComponent {
  title = 'ten-web';
}
